#!/skatvnas3/doc_root/testvectors/searchapp/venv/bin/python

import os
from flask import Flask, request
app = Flask(__name__)

VECTOR_ROOT = "/skatvnas3/doc_root/testvectors/"
URL_ROOT = "http://testvector.jb.man.ac.uk/"

def extract(filename):
    fields = filename.split("_")[:-1]
    fields.pop(1)
    return fields

def search_latest(filename):
    fields = extract(filename)
    vector_type = fields[0]
    search_dir = os.path.join(VECTOR_ROOT, vector_type, "latest")
    latest_vectors = os.listdir(search_dir)
    for this_vector in latest_vectors:
        vector_fields = extract(this_vector)
        if fields == vector_fields:
            return this_vector
    return "None"


@app.route("/")
def query():

    # Grab search terms from URL
    vector_type = request.args.get('type')
    freq = float(request.args.get('freq'))
    githash = request.args.get('version')
    duty = float(request.args.get('duty'))
    dm = request.args.get('dm')
    acc = float(request.args.get('acceleration'))
    shape = request.args.get('shape')
    noise_seed = request.args.get('seed')
    sn = float(request.args.get('sn'))

    # Construct filename from search terms according to filename convention
    filename = vector_type + "_" + githash + "_" + str(freq) + "_" + str(duty) + "_" + str(dm) + "_" + str(acc) + "_" + str(shape) + "_" + str(sn) + "_" + str(noise_seed) + ".fil"

    # Case 1 - we don't know the version or the noise seed
    # In this case we just search the "latest" directory for a file which matches 
    # the user's request. If it doesn't exist in "latest", it doesn't exist anywhere.
    if githash == "None" and noise_seed == "None":
        this_string = search_latest(filename)
        return this_string

    # Case 2 - we know both the version and the noise seed we want. 
    # We just want to check the file is still there. 
    return url

if __name__ == "__main__":
    app.run()
